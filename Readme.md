# AlKamel MQQT Automatization Scripts and documentation
This repo contains the tools used for the generation of the rules in EMQx and its connections to other services like Kafka and TinyVerse. It tries to be very reusable but some fields (like addresses) are hard coded.

## Contents
### Folder emqx
Here are all the scripts related to the EMQx MQTT queue manager. Two files are particularly important:

* rules.txt contains the rules that would be applied for linking to kafka. The format is csv, for instance `cnf/+/getRallyIdRequest,config.getrallyId_request,getRallyIdRequest`, where the 1st field is the MQTT topic, the 2nd the Kafka Topic and the 3rd the name that will appear in the EMQx interface for the rule. Pay special attention to messages like dots in names/fields not allowed, a very easy to fix errror.
* create_rules.sh is the script that creates the bridge to the cloud kafka account and processes `rules.txt`. In case of any mistake, all commands are shown sequentally, for easier depuration.

### Folder Lambda $\Lambda$
Here are some files for quicker configuration of the bridges of the lambda functions. They require the installation of the AWS CLI (https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) (already configured in the cloud machines).
* topics_kafka.txt contains the list of kafka topics in a single column fashion.
* `create_topics.sh` processes topics_kafka and creates mappings between the lambda functions and the kafka endpoints.