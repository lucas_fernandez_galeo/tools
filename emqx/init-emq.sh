IP_ADDRESS=$( ip a show dev eth0 | grep inet | grep 172 | sed 's#/# #g' | awk '{print $2}' )
echo $IP_ADDRESS
sudo sed -i "s#127.0.0.1#$IP_ADDRESS#g" /etc/emqx/emqx.conf
#sudo sed -i "s/## cluster.mcast.addr = 239.192.0.1/cluster.mcast.addr = $IP_ADDRESS/g" /etc/emqx/cluster.conf
